
import { MetaFunctionDescriptionValidation }
  from "../../src/bops-functions/installation/functions-configuration-validation";
import { ModuleKind } from "../../src/bops-functions/installation/functions-installer";
import { expect } from "chai";
import { purgeTestPackages, testFunctionFileSystem, testInstaller } from "../test-managers";

describe("BOps Function Configuration Validator", () =>{
  const testFunctionName = "bops-function-hello-world";
  const testFunctionVersion = "1.1.1";

  afterEach(purgeTestPackages);

  it("Validates successfully a valid meta-function.json", async () => {
    await testInstaller.install(testFunctionName, testFunctionVersion, ModuleKind.NPM);


    const metaFunctionContent = await testFunctionFileSystem
      .getDescriptionFile(testFunctionName, "function");

    const validator = new MetaFunctionDescriptionValidation(metaFunctionContent);

    const execution = () : unknown => validator.validate();

    expect(execution).to.not.throw;
  });

  it("Validates successfully a valid meta-function.json", async () => {
    await testInstaller.install(testFunctionName, testFunctionVersion, ModuleKind.NPM);

    const metaFunctionContent = await testFunctionFileSystem
      .getDescriptionFile(testFunctionName, "function");

    const validator = new MetaFunctionDescriptionValidation(metaFunctionContent);

    validator.validate();

    expect(validator.getFunctionConfiguration().functionName).to.be.equal(testFunctionName);
    expect(validator.getFunctionConfiguration().version).to.be.equal(testFunctionVersion);
  });
});

export enum SchemasFunctions {
  create = "create",
  getById = "getById",
  get = "get",
  updateById = "updateById",
  update = "update",
  deleteById = "deleteById",
  delete = "delete"
}

<p align="center">
  <img src="https://user-images.githubusercontent.com/13098424/131416580-f6718c24-5687-4016-8801-44c177a70d42.png">
</p>

# Meta-System - A system to be any system

- [Documentation](https://mapikit.github.io/meta-system-docs/)
- [Roadmap](https://github.com/mapikit/meta-system/blob/master/ROADMAP.md)

## What is Meta-System?
Meta-System is a software made for simplifying the creation of any system, with any level of complexity. You can create entities, business rules, and set the way to interact with them.

Meta-System unifies the practicality of a no-code software with the flexibility of a functional programming language.

See more at the [Documentation](https://mapikit.github.io/meta-system-docs/) page.

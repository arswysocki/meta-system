# Meta-System Roadmap

## 0.2 - Gemini :heavy_check_mark: 
- :heavy_check_mark: Support to Function Packages;
- :heavy_check_mark: Introduces external protocols;
- :heavy_check_mark: Protocols can provide their BOps functions as well;
- :heavy_check_mark: BETA - Verification of modules dependency property accesses;

### 0.3 - Future Release
- Meta-System configuration can be split in multiple files, as long as there is a path declared;
- Introduces DBProtocols - Databases becomes part of the protocol definition;
- Schemas need to define a protocol Database to connect to;
- Add StdOut Debugging capabilities to BOps;

### 0.4 - Future Release
- Streamline CLI usage;
- Meta-System as a framework - You can create modules locally and use them directly on your Meta-System BOps;
- Polish Meta-System integration to existing Systems;
- Add eventful debugging capabilities to BOps - Integrating Meta-System to an existing system should give you the ability to listen to what is happening inside the engine.
- Verification of module dependecy property accesses - Becoming a stable feature;

### 0.5 - Future Release
- Meta-System can be run on the Browser;

### 1.0 - First Stable Release
...
